#include "core.h"
#ifndef __SYNTHESIS__
#include <iostream>
#endif

ap_int<12> ped_SSR[NCHAN] = {PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT, PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT,PED_INIT, PED_INIT};
ap_int<5> accum_SSR[NCHAN] = {0};
ap_uint<6> channel_counter = 0;


void ped_sub(hls::stream<ap_axis_str> &input_stream, hls::stream<ap_axis_str> &output_stream){
#pragma HLS INTERFACE axis port=input_stream
#pragma HLS INTERFACE axis port=output_stream
#pragma HLS INTERFACE ap_ctrl_none port=return

	#ifndef __SYNTHESIS__
	std::cout<<"channel "<<channel_counter<<"\n";
	#endif
	ap_axis_str adc_value;
	ap_uint<6> channel = channel_counter;
	ap_int<16> pedestal = ped_SSR[channel];
	ap_int<5> accumulator = accum_SSR[channel];
	int to_add = 0;


	for (int i=0; i<NDATA; i++){
		adc_value = input_stream.read();
		to_add = (adc_value.data > pedestal) ? 1:((adc_value.data == pedestal) ? 0:-1);
		accumulator += to_add;
		adc_value.data -= pedestal;

		if (accumulator == 10){
			accumulator = 0;
			pedestal += 1;
		} else if (accumulator == -10){
			accumulator = 0;
			pedestal -=1;
		}
		output_stream.write(adc_value);
	}

	#ifndef __SYNTHESIS__
	std::cout<<"pedestal "<<pedestal<<"\n";
	std::cout<<"accumulator "<<accumulator<<"\n\n";
	#endif

	channel_counter ++;
	if (channel_counter == NCHAN){
		channel_counter = 0;
	}
	ped_SSR[channel] = pedestal;
	accum_SSR[channel] = accumulator;

}
