#ifndef core_h
#define core_h

#include <ap_int.h>
#include <ap_axi_sdata.h>
#include <hls_stream.h>

typedef ap_int<16> word;

#define NDATA 64
#define NCHAN 64
#define PED_INIT 500

typedef ap_axis<16, 1, 0, 0> ap_axis_str;


void ped_sub(hls::stream<ap_axis_str> &input_stream, hls::stream<ap_axis_str> &output_stream);

#endif
